<?php
return [
    "apiKey"      => '', //apiKey
    "apiHost"     => '', //接口域名
    "temperature" => 1, //介于 0 和 1 之间，越大回答问题越灵活
    "max_tokens"  => 1000, //每次回答的最大字符长度
    "model"       => 'gpt-3.5-turbo', //要使用的模型的(支持gpt-4)
    "stream"      => true //如果设置true则流式输出 false则一次性返回
];