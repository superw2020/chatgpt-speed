<?php
namespace app\ai\controller;

use app\BaseController;
use think\Request;
use think\facade\Db;

class Index extends BaseController
{
	public function index()
	{
		return view('');
	}

    public function getChatMsgs(Request $request)
    {
        $group_id = $request->param('group_id');
        $ip = $request->ip();
        $where = [
            ['ip','=',$ip],
            ['group_id','=',$group_id]
        ];
        $res = Db::table('ai_chat_msgs')->field('id,message,response')->where($where)->order('id asc')->select();
        $msgs = [];
        foreach ($res as $key => $msg) {
            $msgs[] = [
                'role' => '我',
                'content' => $msg['message']
            ];
            $msgs[] = [
                'role' => 'ai',
                'content' => $msg['response']
            ];
        }
        return success('获取成功',$msgs);
    }

	public function send(Request $request)
	{
		header('Content-Type: text/event-stream');
      	header('Cache-Control: no-cache');
      	header('Connection: keep-alive');
      	header('X-Accel-Buffering: no');
      	
      	$apiKey = config("open.apiKey");
      	$apiUrl = config("open.apiHost").'/v1/chat/completions';

        //提问数据
        $message = $request->param('message')??'hello!';
        //分组
        $group_id = $request->param('group_id');
        //客户端ip
        $ip = $request->ip();

        // 连续对话需要带着上一个问题请求接口
        $lastMsg = Db::table('ai_chat_msgs')->where([
            ['ip', '=', $ip],
            ['group_id', '=', $group_id],
            ['ctime', '>', (time() - 300)]
        ])->order('id desc')->find();
        // 如果超长，就不关联上下文了
        if ($lastMsg && (mb_strlen($lastMsg['message']) + mb_strlen($lastMsg['response']) + mb_strlen($message) < 3800)) {
            $messages[] = [
                'role' => 'user',
                'content' => $lastMsg['message']
            ];
            $messages[] = [
                'role' => 'assistant',
                'content' => $lastMsg['response']
            ];
        }
        $messages[] = [
            'role'=>'user',
            'content'=>$message
        ];

        //返回数据
        $response = '';
        //不完整的数据
        $imperfect = '';
      	$callback = function($ch, $data) use ($message, $ip, $group_id){
            global $response, $imperfect;
            $dataLength = strlen($data);
            //有可能会返回不完整的数据
            if($imperfect){
                $data = $imperfect . $data;
                $imperfect = '';
            }else{
                if (substr($data, -1) !== "\n") {
                    $imperfect = $data;
                    return $dataLength;
                }
            }

            $complete = @json_decode($data);
            if(isset($complete->error)){
                echo 'data:'.$complete->error->code."\n\n";
                ob_flush();flush();
                exit;
            }
            $pword = $this->parseData($data);
            $word = str_replace("\n", '<br/>', $pword);
            $word = str_replace(" ", '&nbsp;', $word);
            if($pword == 'data: [DONE]' || $pword == 'data: [CONTINUE]'){
                if (!empty($response)) {
                    Db::table('ai_chat_msgs')->insert([
                        'ip' => $ip,
                        'group_id' => $group_id,
                        'message' => $message,
                        'response' => $response,
                        'ctime' => time()
                    ]);
                    $response = '';
                }
                ob_flush();flush();
            }else{
                $response .= $word;
                echo "data:".$word."\n\n";
                ob_flush();flush();
            }
            return $dataLength;
        };

        $postData = [
            'model' => config("open.model"),
            'messages' => $messages,
            'temperature' => config("open.temperature"),
            'max_tokens' => config("open.max_tokens"),
            'frequency_penalty' => 0,
            'presence_penalty' => 0.6,
            'stream' => true
        ];

        $headers  = [
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $apiKey
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($ch, CURLOPT_WRITEFUNCTION, $callback);
        curl_exec($ch);
        exit;
	}

    private function parseData($data)
    {
        $data = str_replace('data: {', '{', $data);
        $data = rtrim($data, "\n\n");

        if(strpos($data, "}\n\n{") !== false) {
            $arr = explode("}\n\n{", $data);
            $data = '{' . $arr[1];
        }

        if (strpos($data, 'data: [DONE]') !== false) {
            return 'data: [DONE]';
        } else {
            $data = @json_decode($data, true);
            if (!is_array($data)) {
                return '';
            }
            if ($data['choices']['0']['finish_reason'] == 'stop') {
                return 'data: [DONE]';
            }
            elseif($data['choices']['0']['finish_reason'] == 'length') {
                return 'data: [CONTINUE]';
            }

            return $data['choices']['0']['delta']['content'] ?? '';
        }

    }
}